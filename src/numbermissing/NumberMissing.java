/*
 * LICENSE
 * plz no use for world war 3
 * plz no sell for $$
 */

package numbermissing;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This parses a file (text.txt) containing numbers and tells you which one is missing.
 * Example:
 * "0, 1, 2, 3, 5, 6" -> "Number 4 is missing"
 * @author Patrick P. (<a href="http://twitter.com/ppati000" target="_blank">Twitter</a> or <a href="mailto:ppati.email@gmail.com">ppati.email@gmail.com</a>)
 * @version 0.0
 * Use at your own risk.
 */
public class NumberMissing {

    /**
     * @param args none
     */
    public static void main(String[] args) {
        
        int expectedNumber = 0;
        
        try {
            Scanner sc = new Scanner(new File("text.txt")).useDelimiter(", ");
            
            while(sc.hasNext()){
                
                int parsedNumber = sc.nextInt();
                if (parsedNumber != expectedNumber){
                    int missingNumbersCount = parsedNumber - expectedNumber;

                    for (int i = 0; i < missingNumbersCount; i++) {
                        System.out.println("Number " + expectedNumber + " is missing!");
                        expectedNumber++;
                    }
                    
                }
                
                expectedNumber++;
            }
            
        } catch (FileNotFoundException ex) {
            System.err.println("Please add a 'text.txt' file and make sure it's readable");
        } catch (InputMismatchException ex) {
            System.err.println("File contained illegal input. Expected: " + expectedNumber);
        }
    }

}
