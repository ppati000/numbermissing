# Download #
[JAR (executable file)](https://bitbucket.org/ppati000/numbermissing/downloads/NumberMissing.jar)

How to use:

- Install Java if not installed already

- Create a file called ```text.txt``` and fill it with numbers seperated by ", " (comma space) or download the sample file from [here](https://bitbucket.org/ppati000/numbermissing/downloads/text.txt)

- Launch the jar from the command line:
```
java -jar NumberMissing.jar

```